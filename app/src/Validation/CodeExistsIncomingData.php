<?php

namespace App\Validation;

use App\Entity\Dto\IncomingSendData;
use App\Repository\Mailing\MailingDataRepository;

/**
 * Class CodeExistsIncomingData
 * @package App\Validation
 */
class CodeExistsIncomingData extends IncomingDataValidator
{
    /**
     * @var MailingDataRepository
     */
    private $mailingDataRepository;

    public function __construct(MailingDataRepository $mailingDataRepository)
    {
        $this->mailingDataRepository = $mailingDataRepository;
    }

    /**
     * @param IncomingSendData $incomingSendData
     * @return bool
     */
    public function check(IncomingSendData $incomingSendData): bool
    {
        if (empty($this->mailingDataRepository->findByCode($incomingSendData->getCode()))) {
            return false;
        }
        return parent::check($incomingSendData);
    }
}