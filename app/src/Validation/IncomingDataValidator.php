<?php

namespace App\Validation;

use App\Entity\Dto\IncomingSendData;

/**
 * Class IncomingDataValidator
 * @package App\Validation
 */
abstract class IncomingDataValidator
{
    /**
     * @var
     */
    private $next;

    /**
     * @param IncomingDataValidator $next
     * @return IncomingDataValidator
     */
    public function linkWith(IncomingDataValidator $next): IncomingDataValidator
    {
        $this->next = $next;
        return $next;
    }

    /**
     * @param IncomingSendData $incomingSendData
     * @return bool
     */
    public function check(IncomingSendData $incomingSendData): bool
    {
        if (!$this->next) {
            return true;
        }
        return $this->next->check($incomingSendData);
    }
}