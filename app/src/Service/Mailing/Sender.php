<?php

namespace App\Service\Mailing;

use App\Entity\Dto\IncomingSendData;
use App\Service\MessageGenerator;
use App\Validation\IncomingDataValidator;
use Psr\Log\LoggerInterface;

/**
 * Class Sender
 * @package App\Service\Mailing
 */
class Sender
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MessageGenerator
     */
    private $generator;

    /**
     * @var IncomingDataValidator
     */
    private $validator;

    /**
     * Sender constructor.
     * @param LoggerInterface $logger
     * @param MessageGenerator $generator
     */
    public function __construct(LoggerInterface $logger, MessageGenerator $generator)
    {
        $this->logger = $logger;
        $this->generator = $generator;
    }

    /**
     * @param string $code
     * @param string $pathToAttachment
     * @return string|void
     */
    public function send(string $code, string $pathToAttachment)
    {
        $sendDataDto = new IncomingSendData($code, $pathToAttachment);
        if (empty($this->validate($sendDataDto))) {
            $this->process($sendDataDto);
            return $this->sendOut();
        } else {
            return "temporary bad answer after validation";
        }
    }

    /**
     * @param IncomingDataValidator $validator
     */
    public function setValidator(IncomingDataValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param IncomingSendData $sendDataDto
     * @return bool
     */
    private function validate(IncomingSendData $sendDataDto): bool
    {
        //TODO: here we should create our chains of responsibility
        if ($this->validator->check($sendDataDto)) {
            //TODO: log this and return something useful!
            return false;
        }
        return true;
    }

    /**
     * @param IncomingSendData $sendDataDto
     */
    private function process(IncomingSendData $sendDataDto)
    {
        $this->generator->setCode($sendDataDto->getCode());
        $this->generator->setAttachment($sendDataDto->getPathToAttachment());
        $this->generator->create();
    }

    private function sendOut()
    {
        // some code for sending email using swiftmailer
    }
}