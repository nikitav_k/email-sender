<?php

namespace App\Entity\Dto;

class IncomingSendData
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $pathToAttachment;

    /**
     * IncomingSendData constructor.
     * @param string $code
     * @param string $pathToAttachment
     */
    public function __construct(string $code, string $pathToAttachment)
    {
        $this->code = $code;
        $this->pathToAttachment = $pathToAttachment;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getPathToAttachment(): ?string
    {
        return $this->pathToAttachment;
    }
}